abstract class Data{
    public abstract void DisplayValue();
}
class Integer extends Data {
    int value;
    Integer(){
        value=100;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
class Long extends Data{
    long value;
    Long(){
        value=2147483647;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class LongFactory extends Factory{
    public Data CreateDataObject(){
        return new Long();
    }
}
class Document {
    Data pd;
    Document(Factory pf) {
        pd=pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new LongFactory());
        d.DisplayData();
    }
}