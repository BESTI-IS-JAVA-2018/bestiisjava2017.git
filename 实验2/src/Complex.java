public class Complex {
    // 定义属性并生成getter,setter
    private double RealPart;
    private double ImagePart;

    public double getterRealPart() {
        return this.RealPart;
    }
    public double getterImagePart() {
        return this.ImagePart;
    }
    public void setterRealPart(double RealPart) {
        this.RealPart = RealPart;
    }
    public void setterImagePart(double ImagePart) {
        this.ImagePart = ImagePart;
    }
    // 定义构造函数
    public Complex() {
        this.RealPart = 0;
        this.ImagePart = 0;
    }
    public Complex(double R,double I) {
        this.RealPart = R;
        this.ImagePart = I;
    }

    //Override Object
    public boolean equals(Object obj) {
        if(this==obj) {
            return true;
        }
        if(!(obj instanceof Complex)) {
            return false;
        }
        Complex complex = (Complex) obj;
        if(getterRealPart()!=complex.getterRealPart()) {
            return false;
        }
        if(getterImagePart()!=complex.getterImagePart()) {
            return false;
        }
        return true;
    }
    public String toString() {
        if(getterImagePart()>0) return getterRealPart() + "+" + getterImagePart() + "i";
        else if(getterImagePart()==0) return  "" + getterRealPart();
        else return getterRealPart() + "" + getterImagePart() + "i";
    }

    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a) {
        this.setterImagePart(this.getterImagePart()+a.getterImagePart());
        this.setterRealPart(this.getterRealPart()+a.getterRealPart());
        return this;
    }
    Complex ComplexSub(Complex a) {
        this.setterImagePart(this.getterImagePart()-a.getterImagePart());
        this.setterRealPart(this.getterRealPart()-a.getterRealPart());
        return this;
    }
    Complex ComplexMulti(Complex a) {
        this.setterImagePart(a.getterImagePart()*this.getterImagePart() - a.getterRealPart()*this.getterRealPart());
        this.setterRealPart(a.getterImagePart()*this.getterRealPart() + a.getterRealPart()*this.getterImagePart());
        return this;
    }
    Complex ComplexDiv(Complex a) {
        Complex c = new Complex();
        if(a.equals(c)) {                                               //注意0
            System.out.println("Error! Dividend can't be 0!");
        }
        else {
            this.setterImagePart(this.getterImagePart()/a.getterImagePart());
            this.setterRealPart(this.getterRealPart()/a.getterRealPart());
        }
        return this;
    }
}