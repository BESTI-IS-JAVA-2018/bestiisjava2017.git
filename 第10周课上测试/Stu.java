import java.util.*;
class StudentKey implements Comparable {
    double d=0;
    String s="";
    StudentKey (double d) {
        this.d=d;
    }
    StudentKey (String s) {
        this.s=s;
    }
    public int compareTo(Object b) {
        StudentKey st=(StudentKey)b;
        if((this.d-st.d)==0)
            return -1;
        else
            return (int)((this.d-st.d)*1000);
    }
}
class StudentIF {
    String name=null;
    int ID=0;
    double math,english,computer,total,aver;
    StudentIF(String s, double m, double e, double f, double a,double b,int c) {
        name=s;
        math=m;
        english=e;
        computer=f;
        total=a;
        aver=b;
        ID=c;
    }
}
public class Stu {
    public static void main(String args[]) {
        TreeMap<StudentKey, StudentIF> treemap = new TreeMap<StudentKey, StudentIF>();
        String str[] = {"岳源","冶宇航", "唐羽瞳", "陈潭飞", "程上杰"};
        int ID[] ={20165337,20165338,20165339,20165301,20165302};
        double math[] = {93,92,98,97,99};
        double english[] = {80,65,81,86,93};
        double computer[] = {93,92,94,96,98};
        double total[] = new double[5];
        double aver[] = new double[5];
        StudentIF student1[] = new StudentIF[5];
        StudentIF student2[] = new StudentIF[5];
        for (int k = 0; k < student1.length; k++) {
            total[k] = math[k] + english[k] + computer[k];
            aver[k] = total[k] / 3;
        }
        for (int k = 0; k < student1.length; k++) {
            student1[k] = new StudentIF(str[k], math[k], english[k], computer[k], total[k], aver[k], ID[k]);
        }
        StudentKey key[] = new StudentKey[5];
        student2= student1;
        for (int k = 0; k < key.length; k++) {
            key[k] = new StudentKey(student1[k].ID);
        }
        for (int k = 0; k < student1.length; k++) {
            treemap.put(key[k], student1[k]);
        }
        for (int k = 0; k < key.length; k++) {
            key[k] = new StudentKey(student1[k].total);
        }
        for (int k = 0; k < student1.length; k++) {
            treemap.put(key[k], student1[k]);
        }
        int number = treemap.size();
        System.out.println("学生有" + number + "个,按总成绩排序:");
        Collection<StudentIF> collection = treemap.values();
        Iterator<StudentIF> iter = collection.iterator();
        int i=0;
        while (iter.hasNext()) {
            StudentIF stu = iter.next();
            System.out.println(" 学号 " + stu.ID + " 姓名 " + stu.name + " 总成绩 " + stu.total);
            i++;
            if(i==5)
                System.out.println("学生共有" + number + "个,按学号排序:");
        }
    }
}

